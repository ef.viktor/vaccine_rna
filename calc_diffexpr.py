import pandas as pd
from rpy2 import robjects
from rpy2.robjects import pandas2ri, Formula
from rpy2.robjects.conversion import localconverter
from rpy2.robjects.packages import importr

from constants import CONTROL_SAMPLES, EXPERIMENTS
from utils.plot_venn import plot_deg_venn

deseq = importr("DESeq2")
to_dataframe = robjects.r('function(x) data.frame(x)')


def convert_signal(row):
    if row["padj"] < 0.05:
        if row["log2FoldChange"] > 1:
            return 1
        elif row["log2FoldChange"] < 1:
            return -1
    return 0


def calc_diff():
    to_concat = list()

    for experiment in EXPERIMENTS:
        count_matrix = pd.read_csv("data/rna_seq/merged_raw.tsv", sep="\t", index_col=0)
        design_matrix = pd.read_csv("data/rna_seq/samples_anno.csv", index_col=0)

        if experiment == "control":
            count_matrix = count_matrix.filter(CONTROL_SAMPLES, axis=1)
            design_matrix = design_matrix.filter(CONTROL_SAMPLES, axis=0)
        elif experiment == "experiment":
            count_matrix = count_matrix.drop(CONTROL_SAMPLES, axis=1)
            design_matrix = design_matrix.drop(CONTROL_SAMPLES, axis=0)
        else:
            raise

        with localconverter(robjects.default_converter + pandas2ri.converter):
            count_matrix = robjects.conversion.py2rpy(count_matrix)
            design_matrix = robjects.conversion.py2rpy(design_matrix)

        dds = deseq.DESeqDataSetFromMatrix(
            countData=count_matrix,
            colData=design_matrix,
            design=Formula("~patient+stage")
        )
        dds = deseq.DESeq(dds)

        # extract contrasts
        with localconverter(robjects.default_converter + pandas2ri.converter):
            deseq_out = deseq.results(dds, contrast=robjects.vectors.StrVector(["stage", "after", "before"]))
            deseq_out = robjects.conversion.rpy2py(to_dataframe(deseq_out))  # back to pandas dataframe

        deseq_out: pd.DataFrame
        deseq_out.index.name = "symbol"
        deseq_out.to_csv(f"output/deseq_{experiment}.tsv", sep="\t")
        deseq_out["signal"] = deseq_out.apply(convert_signal, axis=1)
        diff_expr = deseq_out.filter(["signal"])
        diff_expr.columns = [experiment, ]
        to_concat.append(diff_expr)

    # merged
    merged_result = pd.concat(to_concat, axis=1)
    filtered = merged_result.loc[(merged_result != 0).any(axis=1)]
    filtered.to_csv("output/diff_expr.tsv", sep="\t")


def plot_venn():
    df = pd.read_csv("output/diff_expr.tsv", sep="\t", index_col=0)
    plot_deg_venn(df)


if __name__ == "__main__":
    calc_diff()
    plot_venn()
