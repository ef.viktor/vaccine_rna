from collections import defaultdict

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.pyplot import gcf
from rpy2 import robjects
from rpy2.robjects import pandas2ri, Formula
from rpy2.robjects.conversion import localconverter
from rpy2.robjects.packages import importr
from sklearn.decomposition import PCA

from constants import CONTROL_SAMPLES, EXPERIMENTS

BLUE = "#4c72af"
ORANGE = "#dd8351"
GREEN = "#55a868"
RED = "#c44e52"


def normalize():
    deseq = importr("DESeq2")
    to_dataframe = robjects.r('function(x) data.frame(x)')

    with localconverter(robjects.default_converter + pandas2ri.converter):
        count_matrix = robjects.conversion.py2rpy(pd.read_csv("data/rna_seq/merged_raw.tsv", sep="\t", index_col=0))
        design_matrix = robjects.conversion.py2rpy(pd.read_csv("data/rna_seq/samples_anno.csv", index_col=0))

    dds = deseq.DESeqDataSetFromMatrix(
        countData=count_matrix,
        colData=design_matrix,
        design=Formula("~patient+stage")
    )
    dds = deseq.DESeq(dds)

    normalized_count_matrix = deseq.counts_DESeqDataSet(dds, normalized=True)
    normalized_count_matrix = to_dataframe(normalized_count_matrix)

    with localconverter(robjects.default_converter + pandas2ri.converter):
        normalized_count_df = robjects.conversion.rpy2py(normalized_count_matrix)

    normalized_count_df.rename(columns=lambda x: x.replace("X", "").replace(".", "-"), inplace=True)
    normalized_count_df = normalized_count_df.apply(lambda x: x + 1)
    normalized_count_df = normalized_count_df.apply(np.log2)
    normalized_count_df.to_csv("data/rna_seq/deseq_normalized.tsv", sep="\t")


def plot_pca():
    normalized_data = pd.read_csv("data/rna_seq/deseq_normalized.tsv", sep="\t", index_col=0).T
    pca = PCA(n_components=2)
    pca_results = pca.fit_transform(normalized_data)
    pca_df = pd.DataFrame(data=pca_results, columns=['pc1', 'pc2'], index=normalized_data.index)
    ratios = pca.explained_variance_ratio_

    plt.figure(figsize=(6, 6), dpi=300)

    plt.title("2 component PCA", fontsize=20)
    plt.xlabel(f"PC 1 ({round(ratios[0] * 100, 2)}%)", fontsize=18)
    plt.ylabel(f"PC 2 ({round(ratios[1] * 100, 2)}%)", fontsize=18)
    sns.color_palette()
    plt.grid()

    colors_dict = {
        "control": BLUE,
        "experiment": ORANGE
    }

    lines = defaultdict(list)

    for sample_name in pca_df.index:
        patient, stage = sample_name.split("-")

        sample_type = "control" if sample_name in CONTROL_SAMPLES else "experiment"

        plt.scatter(
            pca_df['pc1'][sample_name],
            pca_df['pc2'][sample_name],
            color=colors_dict[sample_type],
            s=70,
            label=sample_name,
            marker={"1": "o", "3": "x"}[stage]
        )
        lines[patient].append((
            pca_df['pc1'][sample_name],
            pca_df['pc2'][sample_name],
            colors_dict[sample_type]
        ))

    for line in lines.values():
        x_values = [x[0] for x in line]
        y_values = [x[1] for x in line]
        color = line[0][2]
        plt.plot(x_values, y_values, color=color)

    legend_elements = [
        Line2D([0], [0], color=BLUE, label="Mock treated"),
        Line2D([0], [0], color=ORANGE, label="Vaccinated"),
        Line2D([0], [0], marker="o", color="black", label="Before vaccination/mock treatment", markersize=8, linestyle="None"),
        Line2D([0], [0], marker="x", color="black", label="After vaccination/mock treatment", markersize=8, linestyle="None"),
    ]
    plt.legend(handles=legend_elements)

    # display
    plt.tight_layout()
    plt.savefig(f"output/pca.png", dpi=300, pad_inches=0.5)
    plt.show()


def plot_dendrogram():
    normalized_data = pd.read_csv("data/rna_seq/deseq_normalized.tsv", sep="\t", index_col=0).T
    experiment_type_labels = pd.Series(
        [BLUE if x in CONTROL_SAMPLES else ORANGE for x in normalized_data.index],
        name="Mock treated/vaccinated",
        index=normalized_data.index
    )

    stage_labels = pd.Series(
        [{
             "1": GREEN,  # green
             "3": RED  # red
         }[x.split("-")[1]] for x in normalized_data.index],
        name="Before/after experiment",
        index=normalized_data.index
    )

    # palette = iter(sns.color_palette(n_colors=64))
    # palette_dict = defaultdict(lambda: next(palette))
    #
    # patient_labels = pd.Series(
    #     [palette_dict[x] for x in normalized_data.index],
    #     name="Patient",
    #     index=normalized_data.index
    # )

    row_colors = pd.concat([experiment_type_labels, stage_labels], axis=1)
    g = sns.clustermap(
        normalized_data, method="complete",
        col_cluster=None, row_colors=row_colors, cbar_pos=None,
        dendrogram_ratio=.3,
        tree_kws=dict(
            linewidths=(1,)
        )
    )

    # add legends
    for label in ["Mock treated", "Vaccinated"]:
        colors_dict = {
            "Mock treated": BLUE,
            "Vaccinated": ORANGE
        }
        g.ax_col_dendrogram.bar(0, 0, color=colors_dict[label], label=label, linewidth=0)
    g.ax_col_dendrogram.legend(
        title="Mock treated/vaccinated", ncol=2,
        bbox_to_anchor=(0.4, 0.89), bbox_transform=gcf().transFigure
    )

    for label in "Before vaccination/mock treatment", "After vaccination/mock treatment":
        colors_dict = {
            "Before vaccination/mock treatment": GREEN,
            "After vaccination/mock treatment": RED
        }
        g.ax_row_dendrogram.bar(0, 0, color=colors_dict[label], label=label, linewidth=0)
    g.ax_row_dendrogram.legend(
        title="Before/after experiment", ncol=2,
        bbox_to_anchor=(0.8, 0.89), bbox_transform=gcf().transFigure
    )

    plt.savefig(f"output/dendrogram.png", dpi=800)
    plt.show()


def plot_vulcano():
    from bioinfokit import visuz
    for experiment in EXPERIMENTS:
        if experiment == "control": continue
        diff_expr = pd.read_csv(f"output/deseq_{experiment}.tsv", sep="\t").dropna()
        visuz.gene_exp.volcano(
            df=diff_expr, lfc="log2FoldChange", pv="padj",
            pv_thr=(0.1, 0.05), sign_line=True
        )


if __name__ == "__main__":
    # normalize()
    # plot_pca()
    plot_dendrogram()
    # plot_vulcano()
