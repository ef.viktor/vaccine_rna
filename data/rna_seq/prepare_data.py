import os

import pandas as pd

DATA_DIR = "raw"


def main():
    to_concat = list()
    for file in os.listdir(DATA_DIR):
        filepath = os.path.join(DATA_DIR, file)
        curr_df = pd.read_csv(filepath, index_col=0, sep="\t")
        to_concat.append(curr_df)

    merged = pd.concat(to_concat, axis=1)
    merged.rename(columns=lambda x: x.split("_S")[0], inplace=True)
    merged = merged.reindex(sorted(merged.columns), axis=1)
    merged.to_csv("merged_raw.tsv", sep="\t")


if __name__ == "__main__": main()
