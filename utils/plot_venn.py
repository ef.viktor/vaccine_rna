# Plot differential gene expression venn diagram using python
# Example dataframe
#            sample1     sample2       sample3
# gene1         1            0            1
# gene2         0            1            0
# gene3        -1            0            0
# gene4         0            0           -1
# gene5         0            1            0

from collections import defaultdict

import matplotlib_venn as vplt
import pandas as pd
from matplotlib import pyplot as plt


def plot_deg_venn(df: pd.DataFrame):
    # setup variables
    regulations = ["up", "down"]
    sets = defaultdict(dict)
    subsets = dict()
    # extract upregulated and downregulated gene counts and subset overlapings
    num_of_subsets = df.shape[1]
    assert num_of_subsets in [2, 3]
    for regulation in regulations:
        for sample in df.columns:
            curr_df = df.filter([sample])
            curr_df = curr_df[curr_df[sample] == {"up": 1, "down": -1}[regulation]]
            sets[regulation][sample] = set(curr_df.index)
        if num_of_subsets == 2:
            subsets[regulation] = vplt._venn2.compute_venn2_subsets(*sets[regulation].values())
        elif num_of_subsets == 3:
            subsets[regulation] = vplt._venn3.compute_venn3_subsets(*sets[regulation].values())
    # plot empty Venn of equal circles
    if num_of_subsets == 2:
        v = vplt.venn2(subsets=[1] * 3, set_labels=df.columns, normalize_to=45)
    elif num_of_subsets == 3:
        v = vplt.venn3(subsets=[1] * 7, set_labels=df.columns, normalize_to=45)
    up_down_labels = list(zip(*subsets.values()))
    for curr_label, new_labels in zip(v.subset_labels, up_down_labels):  # noqa
        curr_label.set_text(
            f"↑: {new_labels[0]}\n"
            f"↓: {new_labels[1]}"
        )
    plt.show()
